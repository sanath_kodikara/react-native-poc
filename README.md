#SETUP
install brew 
if brew installed - brew update
#detect changes in files in file system
brew install watchman
#install the cli for react native
npm install -g react-native-cli 

#IOS
make sure xcode is up to date
#start the simulator
react-native run-ios

#ANDROID
#set up environment variables 
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_161.jdk/Contents/Home
export ANDROID_SDK_ROOT=/Users/sanathkodikara/Library/Android/sdk
export ANDROID_HOME=$ANDROID_SDK_ROOT

#list of avds available in your system
$ANDROID_HOME/emulator/emulator -list-avds

#start android emulator from command line:
$ANDROID_HOME/emulator/emulator -avd <NAME_OF_YOUR_AVD>

#start the simulator
react-native run-android